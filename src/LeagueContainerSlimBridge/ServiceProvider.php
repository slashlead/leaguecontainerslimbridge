<?php
namespace Slashlead\Component\LeagueContainerSlimBridge;

use League\Container\ServiceProvider\AbstractServiceProvider;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\CallableResolver;
use Slim\Handlers\PhpError;
use Slim\Handlers\Error;
use Slim\Handlers\NotFound;
use Slim\Handlers\NotAllowed;
use Slim\Handlers\Strategies\RequestResponse;
use Slim\Http\Environment;
use Slim\Http\Headers;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Interfaces\CallableResolverInterface;
use Slim\Interfaces\Http\EnvironmentInterface;
use Slim\Interfaces\InvocationStrategyInterface;
use Slim\Interfaces\RouterInterface;
use Slim\Collection;
use Slim\Router;

/**
 * @author Samuel Carlier
 */
class ServiceProvider extends AbstractServiceProvider
{
    /**
     * @var array
     */
    private $userSettings = [];

    /**
     * @var array
     */
    protected $provides = [
        'settings',
        'environment',
        'request',
        'response',
        'router',
        'foundHandler',
        'errorHandler',
        'notFoundHandler',
        'notAllowedHandler',
        'callableResolver',
        'phpErrorHandler'
    ];

    /**
     * @var array
     */
    private $defaultSettings = [
        'httpVersion' => '1.1',
        'responseChunkSize' => 4096,
        'outputBuffering' => 'append',
        'determineRouteBeforeAppMiddleware' => false,
        'displayErrorDetails' => false,
        'addContentLengthHeader' => true,
        'routerCacheFile' => false,
    ];

    /**
     * @param array $userSettings , override default settings
     */
    public function __construct($userSettings = [])
    {
        $this->userSettings = $userSettings;
    }

    /**
     * services taken from Slim\DefaultServicesProvider
     *
     * Use the register method to register items with the container via the
     * protected $this->container property or the `getContainer` method
     * from the ContainerAwareTrait.
     *
     * @return void
     */
    public function register()
    {
        /**
         * settings SLIM configuration
         */
        $this->container->share('settings', function () {
            return new Collection(array_merge($this->defaultSettings, $this->userSettings));
        });

        /**
         * This service MUST return a shared instance
         * of \Slim\Interfaces\Http\EnvironmentInterface.
         *
         * @return EnvironmentInterface
         */
        $this->container->share('environment', function () {
            return new Environment($_SERVER);
        });

        /**
         * PSR-7 Request object
         *
         * @param Container $container
         *
         * @return ServerRequestInterface
         */
        $this->container->add('request', function () {
            return Request::createFromEnvironment($this->container->get('environment'));
        });

        /**
         * PSR-7 Response object
         *
         * @param Container $container
         *
         * @return ResponseInterface
         */
        $this->container->add('response', function () {
            $headers  = new Headers(['Content-Type' => 'text/html; charset=UTF-8']);
            $response = new Response(200, $headers);

            return $response->withProtocolVersion($this->container->get('settings')['httpVersion']);
        });

        /**
         * This service MUST return a SHARED instance
         * of \Slim\Interfaces\RouterInterface.
         *
         * @param Container $container
         *
         * @return RouterInterface
         */
        $this->container->share('router', function () {
            $routerCacheFile = false;
            if (isset($this->container->get('settings')['routerCacheFile'])) {
                $routerCacheFile = $this->container->get('settings')['routerCacheFile'];
            }

            return (new Router)->setCacheFile($routerCacheFile);
        });

        /**
         * This service MUST return a SHARED instance
         * of \Slim\Interfaces\InvocationStrategyInterface.
         *
         * @return InvocationStrategyInterface
         */
        $this->container->share('foundHandler', function () {
            return new RequestResponse;
        });

        /**
         * This service MUST return a callable
         * that accepts three arguments:
         *
         * 1. Instance of \Psr\Http\Message\ServerRequestInterface
         * 2. Instance of \Psr\Http\Message\ResponseInterface
         * 3. Instance of \Error
         *
         * The callable MUST return an instance of
         * \Psr\Http\Message\ResponseInterface.
         *
         * @param Container $container
         *
         * @return callable
         */
        $this->container->add('phpErrorHandler', function () {
            return new PhpError($this->container->get('settings')['displayErrorDetails']);
        });

        /**
         * This service MUST return a callable
         * that accepts three arguments:
         *
         * 1. Instance of \Psr\Http\Message\ServerRequestInterface
         * 2. Instance of \Psr\Http\Message\ResponseInterface
         * 3. Instance of \Exception
         *
         * The callable MUST return an instance of
         * \Psr\Http\Message\ResponseInterface.
         *
         * @param Container $container
         *
         * @return callable
         */
        $this->container->add('errorHandler', function () {
            return new Error($this->container->get('settings')['displayErrorDetails']);
        });

        /**
         * This service MUST return a callable
         * that accepts two arguments:
         *
         * 1. Instance of \Psr\Http\Message\ServerRequestInterface
         * 2. Instance of \Psr\Http\Message\ResponseInterface
         *
         * The callable MUST return an instance of
         * \Psr\Http\Message\ResponseInterface.
         *
         * @return callable
         */
        $this->container->add('notFoundHandler', function () {
            return new NotFound;
        });

        /**
         * This service MUST return a callable
         * that accepts three arguments:
         *
         * 1. Instance of \Psr\Http\Message\ServerRequestInterface
         * 2. Instance of \Psr\Http\Message\ResponseInterface
         * 3. Array of allowed HTTP methods
         *
         * The callable MUST return an instance of
         * \Psr\Http\Message\ResponseInterface.
         *
         * @return callable
         */
        $this->container->add('notAllowedHandler', function () {
            return new NotAllowed;
        });

        /**
         * Instance of \Slim\Interfaces\CallableResolverInterface
         *
         * @param Container $container
         *
         * @return CallableResolverInterface
         */
        $this->container->add('callableResolver', function () {
            return new CallableResolver($this->container);
        });
    }
}
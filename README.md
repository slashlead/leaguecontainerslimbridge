
Bridge to implement league/container in slim
# Installation

    composer require slashlead/configuration

### How to use

    <?php
    
    $container = new \League\Container\Container();
    $container->addServiceProvider(new Slashlead\Component\LeagueContainerSlimBridge\ServiceProvider);
    
    $app = new \Slim\App($container);